# Variation in activity and movement across 17 strains of Trichogramma cacoeciae in relation to temperature and period of the day

This project contains all the different files used to obtain the results and figures presented in Cointe et al. 2023.
"Variability of dispersal for 17 strains of Trichogramma cacoeciae in relation to different abiotic conditions".

An R script called "Script_Small_Arena_2023" with all the codes from the analyses to the different graphs.
An R script called "Script_Ctrax_MoveR_2023" to obtain all the metrics used for the analysis: speed, sinuosity, activity rate.

And two csv files. One entitled "Data_MiniMaze_2021.csv" to obtain the data and graphs for all the presented results in the paper except the PCA with the K-means method.
This last analysis has been run with the following data set : "ForPCA2021".

The files "NuageDepointsDetec.xlsx" and "Ctrax_Detection.xlsx" are the data sets used to obtained the FigS3. on the detections performed by Ctrax.

## License

Gnu general public license

